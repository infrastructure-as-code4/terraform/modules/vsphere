package test

import (
	"github.com/gruntwork-io/terratest/modules/terraform"
	test_structure "github.com/gruntwork-io/terratest/modules/test-structure"
	"testing"
)

func TestStaticVMs(t *testing.T){
	t.Parallel()

	exampleDir :=  test_structure.CopyTerraformFolderToTemp(t, "../", "examples/multiple-static-vm/")

	defer test_structure.RunTestStage(t, "teardown", func() {
		terraformOptions := test_structure.LoadTerraformOptions(t, exampleDir)
		terraform.Destroy(t, terraformOptions)
	})
	test_structure.RunTestStage(t, "setup", func() {
		terraformOptions := configureTerraformOptions(t, exampleDir, true)
		test_structure.SaveTerraformOptions(t, exampleDir, terraformOptions)
		terraform.InitAndApply(t, terraformOptions)
	})
	test_structure.RunTestStage(t, "validate", func() {
		terraformOptions := test_structure.LoadTerraformOptions(t, exampleDir)

		testSSHToHosts(t, terraformOptions)
	})
}
