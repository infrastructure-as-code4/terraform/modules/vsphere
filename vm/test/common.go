package test

import (
	"fmt"
	"github.com/gruntwork-io/terratest/modules/random"
	"github.com/gruntwork-io/terratest/modules/retry"
	"github.com/gruntwork-io/terratest/modules/ssh"
	"github.com/gruntwork-io/terratest/modules/terraform"
	"strings"
	"testing"
	"time"
)

func configureTerraformOptions(t *testing.T, exampleFolder string, staticIPs bool) *terraform.Options {
	uniqueId := random.UniqueId()

	folder := ""
	username :=	"ubuntu"
	password := "ubuntu"

	nameStart := fmt.Sprintf("terratest-%s", uniqueId)
	//instances := random.Random(2,4)
	instances := 2
	networks := make(map[string][]string)

	terraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{})

	if !staticIPs {
		for a := 0; a < instances; a++ {
			networks["Temporary"] = append(networks["Temporary"], "")
		}
		terraformOptions = terraform.WithDefaultRetryableErrors(t, &terraform.Options{
			TerraformDir: exampleFolder,

			Vars: map[string]interface{}{
				"instances":	instances,
				"folder":		folder,
				"networks":		networks,
				"name_start":	nameStart,
				"ssh_username":	username,
				"ssh_password":	password,
			},
		})
	} else {
		randInt := random.Random(10, 250)
		for a := 0; a < instances; a++ {
			networks["Temporary"] = append(networks["Temporary"], fmt.Sprintf("172.32.255.%d", randInt + a))
		}
		gateway := "172.32.255.1"

		terraformOptions = terraform.WithDefaultRetryableErrors(t, &terraform.Options{
			TerraformDir: exampleFolder,

			Vars: map[string]interface{}{
				"instances":	instances,
				"folder":		folder,
				"networks":		networks,
				"gateway":		gateway,
				"name_start":	nameStart,
				"ssh_username":	username,
				"ssh_password":	password,
			},
		})
	}
	return terraformOptions
}


func testSSHToHosts(t *testing.T, terraformOptions *terraform.Options){
	ipAddresses := terraform.OutputList(t, terraformOptions, "virtual_machine_ip_addresses")
	var hosts []ssh.Host
	for _, host := range ipAddresses {
		hosts = append(hosts, ssh.Host{
			Hostname: host,
			Password: terraformOptions.Vars["ssh_password"].(string),
			SshUserName: terraformOptions.Vars["ssh_username"].(string),
		})
	}
	maxRetries := 30
	timeBetweenRetries := 5 * time.Second

	expectedText := "This is a test"
	command := fmt.Sprintf("echo -n '%s'", expectedText)
	for _, host := range hosts {
		description := fmt.Sprintf("SSH to public host %s", host.Hostname)
		retry.DoWithRetry(t, description, maxRetries, timeBetweenRetries, func() (string, error) {
			actualText, err := ssh.CheckSshCommandE(t, host, command)

			if err != nil {
				return "", err
			}

			if strings.TrimSpace(actualText) != expectedText {
				return "", fmt.Errorf("Expected SSH command to return '%s' but got '%s'", expectedText, actualText)
			}

			return "", nil
		})
	}

	expectedText = "This is a test"
	command = fmt.Sprintf("echo -n '%s' && exit 1", expectedText)
	for _, host := range hosts {
		description := fmt.Sprintf("SSH to public host %s with error command", host.Hostname)
		retry.DoWithRetry(t, description, maxRetries, timeBetweenRetries, func() (string, error) {
			actualText, err := ssh.CheckSshCommandE(t, host, command)

			if err == nil {
				return "", fmt.Errorf("Expected SSH command to return an error but got none")
			}

			if strings.TrimSpace(actualText) != expectedText {
				return "", fmt.Errorf("Expected SSH command to return '%s' but got '%s'", expectedText, actualText)
			}

			return "", nil
		})
	}
}