variable "vsphere_server" {
  type = string
}
variable "vsphere_unverified_ssl" {
  type = bool
  default = false
}
variable "vsphere_username" {
  type = string
}
variable "vsphere_password" {
  type = string
  sensitive = true
}