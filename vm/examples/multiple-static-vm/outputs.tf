output "dns_domain" {
  value = var.domain
}
output "virtual_machine_hostnames" {
  value = module.virtual_machines.names
}
output "virtual_machine_ip_addresses" {
  value = module.virtual_machines.default_ip_addresses
}
output "virtual_machine_uuids" {
  value = module.virtual_machines.uuids
}