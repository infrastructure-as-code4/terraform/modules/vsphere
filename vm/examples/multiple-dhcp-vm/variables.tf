variable "domain" {
  default = "localhost.local"
  type = string
}
variable "networks" {
  description = "Define PortGroup and IPs for each VM"
  type        = map(list(string))
  default     = {}
}
variable "name_start" {
  default = ""
  type = string
}
variable "template" {
  default = ""
  type = string
}
variable "resource_pool" {
  default = ""
  type = string
}
variable "instances" {
  default = 1
  type = number
}
variable "datastore" {
  default = ""
  type = string
}
variable "datacenter" {
  default = ""
  type = string
}
variable "folder" {
  default = "terraform"
  type = string
}
variable "dns_servers" {
  default = ["1.1.1.1", "8.8.8.8"]
  type = list(string)
}
variable "gateway" {
  default = ""
  type = string
}
variable "resources" {
  type = object({
    cpu = number
    ram = number
  })
  default = {
    cpu = 2
    ram = 4096
  }
}
variable "credentials" {
  type = object({
    username = string
    password = string
  })
  default = {
    username = "ubuntu"
    password = "ubuntu"
  }
}

variable "ssh_username" {
  type = string
}
variable "ssh_password" {
  type = string
}