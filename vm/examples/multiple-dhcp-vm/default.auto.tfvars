datacenter = "Datacenter"
datastore = "HPDS01"
resource_pool = "TerraTest"
instances = 3

name_start = "example-dhcp-"

#vsphere_credentials = {
#  username = "administrator@vsphere.local"
#  password = "1Pa$$word"
#}
vsphere_server = "vcsa.sturla.uk"
vsphere_unverified_ssl = true

template = "Ubuntu Server 20.04 Template"

networks = {
  "Temporary" = ["", "", ""]
}
gateway = "172.32.255.1"