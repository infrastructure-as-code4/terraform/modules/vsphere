module "virtual_machines" {
  source = "../../"
  instances = var.instances

  datacenter = var.datacenter

  datastore = var.datastore
  resource_pool = var.resource_pool

  template = var.template

  name = var.name_start
  resources = var.resources

  domain = var.domain
  networks = var.networks

  gateway = var.gateway
  dns_servers = var.dns_servers
  folder = var.folder
}